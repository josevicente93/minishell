- Cuando se hace background, el resultado aparece justo después de mostrar el prompt

- Línea que se ha utilizado para compilar: gcc version_buena.c libparser_64.a -o practica2 
- Comando que se ha utilizado para ver las entradas abiertas en la tabla de descriptores de fichero:
lsof -r 1 -c practica2 | grep -i -e FIFO -e = -e COMMAND -e PID -e USER -e FD -e TYPE -e DEVICE -e SIZE/OFF -e NODE -e NAME

- Datos de interés:
 Líneas de código: 286
 Número de comentarios: 53
 Número total de líneas: 307
 Porcentaje de comentarios: 15.634218%

- ps al --> sirve para ver si hay procesos zombie

- xman & --> manual visual

