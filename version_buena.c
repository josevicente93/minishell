#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include "parser.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>

// Variables globales
tline * tipoMan; //Variable que representa el mandato
int * pids; //Lista donde estarán los pids de los hijos

struct sTub{
	int tub[2];
};
typedef struct sTub TPipe;
TPipe *pipes;

//Funciones
int existenLosMandatos (tline * tipoMan){
	int i;
	
	for (i=0; i<tipoMan->ncommands; i++) {
		if(tipoMan->commands[i].filename == NULL){
			/*Si el mandato introducido no existe devuelve el valor de i.
			Si el mandato sí existe, devuelve -1.
			*/ 
			return i;
		}
	}
	return -1; 
}

// -------------------------------------------------------------------

int esCd(tline * tipoMan){
	if(!strcmp(tipoMan->commands[0].argv[0],"cd")) 
		return 1;
	return 0;
}

// -------------------------------------------------------------------

void cd (char * directorio){
	char * path = "";
	if(directorio == NULL) path = getenv("HOME");
	else path = directorio;
	int check = chdir(path);
	if(check == -1)
		fprintf(stderr,"Error: %s \n",strerror(errno));
	else{
		//Guardo el directorio de trabajo actual en la variable cwd y la imprimo por pantalla
		char cwd[1024];
		getcwd(cwd, sizeof(cwd));
		fprintf(stderr,"cd: %s \n",cwd);
	} 
}

// -------------------------------------------------------------------

int hacerRedirecciones(tline * tipoMan){
	int df = 0;
	if(getpid() == pids[(tipoMan->ncommands)-1])
		if(tipoMan->redirect_input != NULL){
			df = open(tipoMan->redirect_input,O_RDONLY); //Consigo el f.d. del fichero
			if(df == -1) fprintf(stderr,"%s: Error. %s \n",tipoMan->redirect_input,strerror(errno));
			else{
				dup2(df,0);
				close(df);
			} 
		}		

	if(getpid() == pids[0]){
		if(tipoMan->redirect_output != NULL ){
			df = creat(tipoMan->redirect_output,S_IWUSR|S_IRUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH); //Consigo el f.d. del fichero 	
			if(df == -1) fprintf(stderr,"%s: Error. %s \n",tipoMan->redirect_output,strerror(errno));
			else{
				dup2(df,1);
				close(df);
			} 
		}

		if(tipoMan->redirect_error != NULL){
			df = creat(tipoMan->redirect_error,S_IWUSR|S_IRUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH); //Consigo el f.d. del fichero 		
			if(df == -1) fprintf(stderr,"%s: Error. %s \n",tipoMan->redirect_error,strerror(errno));
			else{
				dup2(df,2);
				close(df);
			}
		}			
	}	

	return df;
}

// -------------------------------------------------------------------

void leerYAnalizar (){
	char buf[1024]; 
	
	while (fgets(buf, 1024, stdin)) {
		
		tipoMan = tokenize(buf);
		if (tipoMan==NULL) {
			continue;
		}
		break;
	}
}

// -------------------------------------------------------------------

void handler (int sig){
	if(sig == SIGINT || sig == SIGQUIT){
		if(tipoMan!=NULL)
			if(tipoMan->background == 0 && tipoMan->ncommands > 0){
				int i;
				for(i = 0; i < tipoMan->ncommands; i++)
					if(pids[i] != 0)
						kill(pids[i],SIGKILL);	 
			}			
	}
}

// -------------------------------------------------------------------

void cerrarPipe (int index){
	close(pipes[index].tub[0]);
	close(pipes[index].tub[1]);	
}

// -------------------------------------------------------------------

void sigchld_handler (int sig){
	int pidAux;
	while((pidAux = waitpid(-1,NULL,WNOHANG)) > 0){} //Mientras haya procesos hijos por los que esperar, espero
	if(pidAux == pids[tipoMan->ncommands - 1]){ //Si es el último hijo...
		free(pipes); //Libero la memoria reservada para los pipes
		free(pids); //Libero la memoria reservada para los pids
	}
}

//Main
int main (int argc, char ** argv){
	//Algunas variables
	pid_t pid; 
	int i;
	int existeValor;

	//Asignación de manejador para las señales SIGINT y SIGQUIT
	if(signal(SIGINT,handler) == SIG_ERR){
		fprintf(stderr,"No se pudo asignar el manejador para la señal SIGINT. \n");
		exit(1);
	} 
	if(signal(SIGQUIT,handler) == SIG_ERR){
		fprintf(stderr,"No se pudo asignar el manejador SIGQUIT. \n");
		exit(1);
	} 
	
	//Cuerpo principal 
	while(1){
		fprintf(stderr,"msh> ");
		leerYAnalizar();
		if(tipoMan->ncommands == 0){ // Si no se ha introducido ningún mandato...
			fprintf(stderr,"Ningún mandato introducido \n");
			continue;
		}
		else{
			existeValor = existenLosMandatos(tipoMan);
			if(existeValor != -1){
				/*Si no existe algún mandato de los introducidos puede ser que efectivamente no exista o que 
				*el mandato introducido sea cd.*/
				if(esCd(tipoMan)){
					char * directorio = tipoMan->commands[0].argv[1];
					cd(directorio);
				}else fprintf(stderr,"%s : No se encuentra el mandato \n",tipoMan->commands[existeValor].argv[0]);
				continue;
			}else{ //Si existen los mandatos introducidos (y tampoco es cd), entonces...
				pipes = (TPipe *) malloc( sizeof(TPipe) * (tipoMan->ncommands-1) );
				pids = (int*) malloc( sizeof(int) * (tipoMan->ncommands) );
				if(tipoMan->ncommands > 1)
					for(i = 0; i<tipoMan->ncommands-1; i++){ // Inicializo los pipes
						pipe(pipes[i].tub);
					}						
				
				for(i = 0; i<tipoMan->ncommands; i++){ // Creo los procesos hijos si soy el padre
					pid = fork();
					if(pid == 0){
						pids[i] = getpid();
						break;
					}
					else pids[i]=pid;
				}
				
				if (pid < 0){
					fprintf(stderr,"Fallo en el fork: %s \n",strerror(errno)); 
					exit(1);
				}
				else{ //Se ha realizado fork corectamente
					if(pid != 0){ //Si soy el padre...
						if(tipoMan->ncommands >= 2){
							int top = (tipoMan->ncommands)*2;
							//Cierro entradas innecesarias
							for(i = 3; i<=top; i++) close(i);
						}
						 
						if(!tipoMan->background){ //Si no se ejecuta en background...
							if(signal(SIGCHLD,SIG_DFL) == SIG_ERR){ //Asigno el manejador por defecto a la señal SIGCHLD 
								fprintf(stderr,"No se pudo asignar el manejador por defecto a SIGCHLD. \n");
								exit(1);		
							}
							for(i = 0; i < tipoMan->ncommands; i++) wait(NULL); /* Espera a que acaben todos los mandatos si 
																				* no se ejecutan en background*/
							free(pipes); //Libero la memoria reservada para los pipes
							free(pids); //Libero la memoria reservada para los pids
						} 
						else{ //Si sí se ejecuta en backrground...
							if(signal(SIGCHLD,sigchld_handler) == SIG_ERR){ //Asigno el manejador sigchld_handler a la señal SIGCHLD 
								fprintf(stderr,"No se pudo asignar el manejador SIGCHLD. \n");
								exit(1);		
							}
							for(i = 0; i < tipoMan->ncommands; i++) fprintf(stderr,"[%i] %i \n",i+1,pids[i]);								
						}						
						continue;
					}
					else{ //Si soy un proceso hijo...
						int aux; //Posición que ocupa el mandato en el array de pids
						int mandato; //Posición del mandato en el array del campo commands de la variable tipoMan
						
						if( pids[(tipoMan->ncommands)-1] == getpid() ){ //El primero en ejecutarse
							aux = (tipoMan->ncommands)-1;
							mandato = 0;
							
							dup2(pipes[aux-1].tub[1],1);
							
							close(pipes[aux-1].tub[0]);

							i = 1;
							if(aux-i >= 0)
								while( (aux - i) != 0 ){
									i++;
									cerrarPipe(aux-i);
								}														
						}
						else{ 
							if(getpid() == pids[0]){ //El último en ejecutarse
								aux = (tipoMan->ncommands)-1;
								mandato = aux;

								dup2(pipes[0].tub[0],0);
								
								close(pipes[0].tub[1]);
								
								i = 1;
								while( i < aux){
									cerrarPipe(i);
									i++;
								}							
							}else{ //Otro hijo que no es ni el primero ni el último
								while( getpid() != pids[aux] && (aux < tipoMan->ncommands) ) aux++;

								mandato = aux;
								
								dup2(pipes[aux].tub[0],0);
								dup2(pipes[aux-1].tub[1],1);

								close(pipes[aux].tub[1]);
								close(pipes[aux-1].tub[0]);
								
								i=0;
								while(i < aux-1){
									cerrarPipe(i);	
									i++;									
								}
								
								i=aux+1;
								while( i < (tipoMan->ncommands-1) ){
									cerrarPipe(i);
									i++;									
								}							
							}
						}
						int redirOk = hacerRedirecciones(tipoMan);
						if(redirOk == -1) exit(1); //Si hay error en la redirección, muestra el prompt	
										
						free(pipes); //Libero la memoria reservada para los pipes
						free(pids); //Libero la memoria reservada para los pids
						
						execvp(tipoMan->commands[mandato].filename,tipoMan->commands[mandato].argv); //Ejecuto la instrucción
					}
				}
			}

		}
	
	}
	return 0;
}
